<?php
$db = null;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task list</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="content">
    <?php 
    try {
        $db = new PDO('mysql:host=localhost;dbname=list;charset-utf-8','taskacc','123');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $pdoex) {
        print "<p>Database could not be opened. " . $pdoex->getMessage() . "</p>";
    }
    ?>