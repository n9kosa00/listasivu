<?php require_once 'inc/top.php'; ?>
<h3>Save</h3>
<?php 
try {
    $description = filter_input(INPUT_POST, 'task', FILTER_SANITIZE_STRING);
    $query = $db->prepare("INSERT INTO task (description) VALUES (:description);");
    $query->bindValue(':description',$description,PDO::PARAM_STR);
    $query->execute();
    print "<p>Task added!</p>";
}
catch (PDOException $pdoex) {
    print "<p>Saving data has failed. " . $pdoex->getMessage() . "</p>";
}
?>
<a href="index.php">Tasklist</a>
<?php require_once 'inc/bottom.php'; ?>